﻿using System;

namespace task24
{
    class Program
    {
        public static int calculateNum(int num1, int num2, int num3)
        {
            return num1*num2*num3;
        }
        
        public static void Main(string[] args)
        {
            Console.WriteLine(calculateNum(5, 5, 1));
            Console.WriteLine(calculateNum(9, 7, 6));
            Console.WriteLine(calculateNum(2, 2, 2));
            Console.WriteLine(calculateNum(8, 32, 15));
            Console.WriteLine(calculateNum(51, 6, 1));
            
        }
    }
}